# nouveau

*a DIY phone*

Currently, only the hardware in the B.O.M. below are supported. Will add support
for more hardware if anyone actually uses this.

**Setup Instructions**





**FAQ**

Q: Is there an operating system?

A: No. I was looking into using either freeRTOS or LuaRTOS, but it would be more trouble than it's worth
to port it to the Atmel 32u4 chip I'm running this project on. Instead, I'm implementing a finite-state machine
program used to run the various functions of the device. This saves memory and makes debugging a lot easier.






Q: What is this programmed in?

A: The GUI is curently programmed in Processing, but will be ported to Arduino's TFT LCD library, to make it
compatible with the display. 


Update (6/28/2019)
===========================

I recently had a thought that struck me: what if I implemented RFID for authentication? No annoying keyfobs or cards, though. As it turns out, the "chips" that get 
implanted into animals at the vet for safe tracking are actually RFID, usually 13.56 MHz band. I'm entertaining the thought of surgically implanting one of these like-chips
into myself, on the specific area along the fold between the thumb and forefinger of the right hand, where there exists no major arteries or veins with the subcutaneous tissue. Once
implanted, this would serve as the key to an always-on RFID reader module, freely available in PCB format on AliExpress or Ebay, which would only allow operation of
the phone while the magnetic field of my right hand is detected. Pretty cool, right? I'm thinking about also adding an RFID module add-on kit to my bike lock, where
the reader would activate a relay that would switch a motor 90 degrees to activate the key, inside a welded-on enclosure.