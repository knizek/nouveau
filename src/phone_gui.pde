import processing.video.*;

PImage img;
PImage sprite;
int px = 60;
public int k = 0;
int x;
int y;
int w = 64;
int fps = 1;
boolean snek = false;
private String state = "home";
String typing = "";
String saved = "";
PFont f;
PImage slime;
Movie mint;

void setup() {
  size(240, 320);
  img = loadImage("alpha_mod.jpg");
  sprite = loadImage("Webp.net-resizeimage.png");
  slime = loadImage("slime.png");
  f = createFont("Arial", 32);
  
}


void draw() {
  update(mouseX, mouseY);
  if (state == "home") {
  background(0);
  image(img, 0, 0);
  text("lorem ipsum", 90, 60);
  //image(sprite, 40, 40);
  x = k;
  y = (k % w) * w;
  copy(sprite, x, y, w, w, 90, 220, w, w);
  if (fps == 30) {
    k++; 
    fps = 0;
  }
  fps++;
  if (k == 3) {
    k = 0;
  }
  } else if (state == "snek") {
    fps = 0;
      background(0);
      fill(100);
      rect(10, 40, 220, 60);
      textFont(f);
      fill(255);
      textAlign(LEFT);
      text(typing, 20, 80);
    } else if (state == "call") {
      background(1000);
      fill(255);
      int w = 843 / 8;
      text("in call...", 60, 60);
        x = k * w;
        y = 0;
        copy(slime, x, y, w, w, 60, 180, w, 112);
  if (fps == 10) {
    k++; 
    fps = 0;
  }
  fps++;
  if (k == 8) {
    k = 0;
  }
    }
  }
  
void update(int x, int y) {
  if (detectMouse(90, 220, w, w) && state != "snek") {
    snek = true;
  } else {
    snek = false;
  }
  
}

void keyTyped() {
  if (state == "call" && key == '~') {
    state = "home";
  }
}

void keyPressed() {
  if (typing.length() > 9) {
    typing = "";
  }
  if (key == '%') {
    typing = "";
    return;
  }
  if (key == '\n') {
    state = "call";
    typing = "";
  } else {
    typing = typing + key;
  }
}

  
boolean detectMouse(int x, int y, int width, int height) {
  if (mouseX >= x && mouseX <= x+width &&
      mouseY >= y && mouseY <= y+height) {
        return true;
      }
  return false;
}
  
void mousePressed() {
  if (snek) {
    state = "snek";
  }
}
  
