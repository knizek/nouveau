package gitlet;

import javax.swing.tree.TreeNode;
import java.io.File;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import edu.princeton.cs.algs4.*;

/* Driver class for Gitlet, the tiny stupid version-control system.
   @author
*/


public class Main {

    /* Usage: java gitlet.Main ARGS, where ARGS contains
       <COMMAND> <OPERAND> .... */
    public static void main(String... args) {

        // TODO: YOUR CODE HERE
    }


    void init() {
        // insert code here
        String[] commits = new String[1000];
        File gitlet = new File("./.gitlet");
        if (gitlet.exists()) {
            throw new IllegalArgumentException("A gitlet version-control system already exists in the current directory.");
        }
        new File("./.gitlet").mkdir();
        SymbolDigraph log = new SymbolDigraph("master", "===");
        log.digraph().addEdge(0, 1);
        commits[0] = "initial commit";

    }

    void add() {
        // insert code here
    }

    void commit() {
        // insert code here
    }

    void branch(String b) {
        //insert code here
    }

    void rm() {
        // insert code here
    }

    void log() {
        // insert code here
    }

    void globalLog() {
        // insert code here
    }

    void find() {
        // insert code here
    }

    void status() {
        // insert code here
    }

    void checkout() {
        // insert code here
    }

    void removeBranch() {
        // insert code here
    }

    void reset() {
        // insert code here
    }

    void merge() {
        // insert code here
    }
}
